# All unpinned blobs below are extracted from cheetah-td1a.220804.031-factory-6152f6f3

# AICore
product/priv-app/AICorePrebuilt/AICorePrebuilt.apk;PRESIGNED

# AmbientStreaming
product/priv-app/AmbientStreaming/AmbientStreaming.apk;PRESIGNED

# AndroidAuto
product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk;PRESIGNED

# Arcore
product/app/arcore/arcore.apk;PRESIGNED

# Audio
product/media/audio/alarms/A_real_hoot.ogg
product/media/audio/alarms/Bright_morning.ogg
product/media/audio/alarms/Cuckoo_clock.ogg
product/media/audio/alarms/Early_twilight.ogg
product/media/audio/alarms/Fresh_start.ogg
product/media/audio/alarms/Full_of_wonder.ogg
product/media/audio/alarms/Gentle_breeze.ogg
product/media/audio/alarms/Icicles.ogg
product/media/audio/alarms/Jump_start.ogg
product/media/audio/alarms/Loose_change.ogg
product/media/audio/alarms/Rolling_fog.ogg
product/media/audio/alarms/Spokes.ogg
product/media/audio/alarms/Sunshower.ogg
product/media/audio/notifications/Beginning.ogg
product/media/audio/notifications/Coconuts.ogg
product/media/audio/notifications/Duet.ogg
product/media/audio/notifications/End_note.ogg
product/media/audio/notifications/Eureka.ogg
product/media/audio/notifications/Gentle_gong.ogg
product/media/audio/notifications/Mallet.ogg
product/media/audio/notifications/Orders_up.ogg
product/media/audio/notifications/Ping.ogg
product/media/audio/notifications/Pipes.ogg
product/media/audio/notifications/Popcorn.ogg
product/media/audio/notifications/Shopkeeper.ogg
product/media/audio/notifications/Sticks_and_stones.ogg
product/media/audio/notifications/Tuneup.ogg
product/media/audio/notifications/Tweeter.ogg
product/media/audio/notifications/Twinkle.ogg
product/media/audio/ringtones/Copycat.ogg
product/media/audio/ringtones/Crackle.ogg
product/media/audio/ringtones/Flutterby.ogg
product/media/audio/ringtones/Hotline.ogg
product/media/audio/ringtones/Leaps_and_bounds.ogg
product/media/audio/ringtones/Lollipop.ogg
product/media/audio/ringtones/Lost_and_found.ogg
product/media/audio/ringtones/Mash_up.ogg
product/media/audio/ringtones/Monkey_around.ogg
product/media/audio/ringtones/Schools_out.ogg
product/media/audio/ringtones/The_big_adventure.ogg
product/media/audio/ringtones/Your_new_adventure.ogg
product/media/audio/ringtones/Zen_too.ogg
product/media/audio/ui/AttentionalHaptics.ogg
product/media/audio/ui/ChargingStarted.ogg
product/media/audio/ui/Dock.ogg
product/media/audio/ui/Effect_Tick.ogg
product/media/audio/ui/InCallNotification.ogg
product/media/audio/ui/KeypressDelete.ogg
product/media/audio/ui/KeypressInvalid.ogg
product/media/audio/ui/KeypressReturn.ogg
product/media/audio/ui/KeypressSpacebar.ogg
product/media/audio/ui/KeypressStandard.ogg
product/media/audio/ui/Lock.ogg
product/media/audio/ui/LowBattery.ogg
product/media/audio/ui/NFCFailure.ogg
product/media/audio/ui/NFCInitiated.ogg
product/media/audio/ui/NFCSuccess.ogg
product/media/audio/ui/NFCTransferComplete.ogg
product/media/audio/ui/NFCTransferInitiated.ogg
product/media/audio/ui/Trusted.ogg
product/media/audio/ui/Undock.ogg
product/media/audio/ui/Unlock.ogg
product/media/audio/ui/VideoRecord.ogg
product/media/audio/ui/VideoStop.ogg
product/media/audio/ui/WirelessChargingStarted.ogg
product/media/audio/ui/audio_end.ogg
product/media/audio/ui/audio_initiate.ogg
product/media/audio/ui/camera_click.ogg
product/media/audio/ui/camera_focus.ogg
product/media/audio/ui/reverse_charging_end.ogg
product/media/audio/ui/reverse_charging_start.ogg

# Browser
product/app/Chrome-Stub/Chrome-Stub.apk;OVERRIDES=webview,Browser2,Jelly;PRESIGNED
product/app/Chrome/Chrome.apk.gz
product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk;PRESIGNED
product/app/TrichromeLibrary/TrichromeLibrary.apk.gz
product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk;OVERRIDES=webview;PRESIGNED
product/app/WebViewGoogle/WebViewGoogle.apk.gz
product/priv-app/ScribePrebuilt/ScribePrebuilt.apk;PRESIGNED

# Carrier
product/etc/permissions/com.android.sdm.plugins.connmo.xml
product/etc/permissions/com.android.sdm.plugins.dcmo.xml
product/priv-app/CarrierLocation/CarrierLocation.apk;PRESIGNED
product/priv-app/CarrierMetrics/CarrierMetrics.apk;PRESIGNED
product/priv-app/CarrierWifi/CarrierWifi.apk;PRESIGNED
product/priv-app/ConnMO/ConnMO.apk;PRESIGNED
product/priv-app/DCMO/DCMO.apk;PRESIGNED
product/priv-app/OdadPrebuilt/OdadPrebuilt.apk;PRESIGNED
product/priv-app/TetheringEntitlement/TetheringEntitlement.apk;PRESIGNED
system_ext/framework/RadioConfigLib.jar
system_ext/priv-app/CarrierSetup/CarrierSetup.apk;PRESIGNED

# CBRS Network Monitor
product/priv-app/CbrsNetworkMonitor/CbrsNetworkMonitor.apk;PRESIGNED

# Charger Animation Resources
vendor/etc/res/images/charger/battery_fail.png:product/etc/res/images/charger/battery_fail.png
vendor/etc/res/images/charger/battery_scale.png:product/etc/res/images/charger/battery_scale.png
vendor/etc/res/images/charger/main_font.png:product/etc/res/images/charger/main_font.png
vendor/etc/res/values/charger/animation.txt:product/etc/res/values/charger/animation.txt

# DMService
product/lib64/libdmengine.so
product/lib64/libdmjavaplugin.so
product/priv-app/DMService/DMService.apk;PRESIGNED

# Fi
product/etc/sysconfig/google_fi.xml

# Files
product/priv-app/FilesPrebuilt/FilesPrebuilt.apk;PRESIGNED
system_ext/priv-app/StorageManagerGoogle/StorageManagerGoogle.apk;OVERRIDES=StorageManager;PRESIGNED

# Flipendo
system_ext/app/Flipendo/Flipendo.apk;PRESIGNED

# Google Play
product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
system/app/GoogleExtShared/GoogleExtShared.apk;OVERRIDES=ExtShared;PRESIGNED
system_ext/priv-app/GoogleFeedback/GoogleFeedback.apk;PRESIGNED
system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED

# Google Camera
product/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml
system_ext/etc/permissions/com.google.android.camera.extensions.xml
system_ext/framework/com.google.android.camera.extensions.jar
vendor/etc/permissions/com.google.android.camera.experimental2023.xml
vendor/framework/com.google.android.camera.experimental2023.jar

# Google Health Connect
product/priv-app/HealthIntelligenceStubPrebuilt/HealthIntelligenceStubPrebuilt.apk;PRESIGNED


# Intelligence
product/priv-app/DeviceIntelligenceNetworkPrebuilt-v.U.14.playstore/DeviceIntelligenceNetworkPrebuilt-v.U.14.playstore.apk:product/priv-app/DeviceIntelligenceNetworkPrebuilt/DeviceIntelligenceNetworkPrebuilt.apk;PRESIGNED

# Keyboard
product/usr/share/ime/google/d3_lms/ko_2018030706.zip
product/usr/share/ime/google/d3_lms/mozc.data
product/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip

# Location
product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk;PRESIGNED

# Lock screen clocks
system_ext/priv-app/SystemUIClocks-BigNum/SystemUIClocks-BigNum.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Calligraphy/SystemUIClocks-Calligraphy.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Flex/SystemUIClocks-Flex.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Growth/SystemUIClocks-Growth.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Inflate/SystemUIClocks-Inflate.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Metro/SystemUIClocks-Metro.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-NumOverlap/SystemUIClocks-NumOverlap.apk;PRESIGNED
system_ext/priv-app/SystemUIClocks-Weather/SystemUIClocks-Weather.apk;PRESIGNED

# Offline VoiceRecognition Models
product/usr/srec/en-US/SODA_punctuation_config.pb
product/usr/srec/en-US/SODA_punctuation_model.tflite
product/usr/srec/en-US/acousticmodel/LARGE_RNNT_VOICE_ACTIONS_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/LARGE_RNNT_VOICE_ACTIONS_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_model
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_model
product/usr/srec/en-US/acousticmodel/SODA_DICTATION_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/SODA_DICTATION_EP_UNIFIED_FRONTEND.endpointer_portable_lstm_model
product/usr/srec/en-US/acousticmodel/SODA_DICTATION_EP_UNIFIED_FRONTEND_LANGID.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/SODA_DICTATION_EP_UNIFIED_FRONTEND_LANGID.endpointer_portable_lstm_model
product/usr/srec/en-US/config.pumpkin
product/usr/srec/en-US/configs/LANGUAGE_IDENTIFICATION.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_CONTINUOUS.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT_compiler.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_VOICE_MATCH.config
product/usr/srec/en-US/configs/ONDEVICE_SPEAKER_TURN_DETECTION.config
product/usr/srec/en-US/context_prebuilt/SODA.prebuilt_class_archive.mfar
product/usr/srec/en-US/context_prebuilt/SODA.prebuilt_class_archive.syms
product/usr/srec/en-US/context_prebuilt/apps.txt
product/usr/srec/en-US/context_prebuilt/contacts.txt
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_car_automation.action.union_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_manual_fixes_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_top_radio_station_frequencies_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/songs.txt
product/usr/srec/en-US/denorm/embedded_class_denorm.mfar
product/usr/srec/en-US/denorm/embedded_covid_19.mfar
product/usr/srec/en-US/denorm/embedded_fix_ampm.mfar
product/usr/srec/en-US/denorm/embedded_normalizer_no_emoticons_no_spoken_punc.mfar
product/usr/srec/en-US/denorm/embedded_normalizer_only_emojis.mfar
product/usr/srec/en-US/denorm/embedded_normalizer_only_spoken_punctuation.mfar
product/usr/srec/en-US/denorm/embedded_replace_annotated_emoticon_words_dash.mfar
product/usr/srec/en-US/denorm/embedded_replace_annotated_punct_words_dash_no_emoticons.mfar
product/usr/srec/en-US/denorm/porn_normalizer_on_device.mfar
product/usr/srec/en-US/denorm/remove_spoken_punc_formatting.mfar
product/usr/srec/en-US/diarization/SODA_SPEAKER_TURN.scd.decoder
product/usr/srec/en-US/diarization/SODA_SPEAKER_TURN.scd.encoder
product/usr/srec/en-US/diarization/SODA_SPEAKER_TURN.scd.joint_model
product/usr/srec/en-US/diarization/SODA_SPEAKER_TURN.scd.mean_stddev
product/usr/srec/en-US/diarization/SODA_SPEAKER_TURN.scd.syms
product/usr/srec/en-US/endtoendmodel/large_continuous_frontend_params.mean_stddev
product/usr/srec/en-US/endtoendmodel/large_short_frontend_params.mean_stddev
product/usr/srec/en-US/endtoendmodel/medium-decoder.tflite
product/usr/srec/en-US/endtoendmodel/medium-encoder.tflite
product/usr/srec/en-US/endtoendmodel/medium-joint_posterior.tflite
product/usr/srec/en-US/endtoendmodel/medium-joint_prior.tflite
product/usr/srec/en-US/endtoendmodel/medium-rank_candidates_acoustic.tflite
product/usr/srec/en-US/endtoendmodel/medium.syms.compact
product/usr/srec/en-US/endtoendmodel/medium.wpm.portable
product/usr/srec/en-US/g2p
product/usr/srec/en-US/g2p.syms
product/usr/srec/en-US/g2p_phonemes.syms
product/usr/srec/en-US/langid/ONDEVICE_langid.tflite
product/usr/srec/en-US/langid/application_params_langid_stream_multiclass_ONDEVICE
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_meanstddev_vector
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_vocabulary.syms
product/usr/srec/en-US/metadata
product/usr/srec/en-US/monastery_config.pumpkin
product/usr/srec/en-US/offline_action_data.pb
product/usr/srec/en-US/pumpkin.mmap
product/usr/srec/en-US/semantics.pumpkin
product/usr/srec/en-US/voice_match/MARBLE_speakerid.tflite
product/usr/srec/en-US/voice_match/MARBLE_voice_filter.tflite

# Overlays
product/overlay/AccessibilityMenu__husky__auto_generated_rro_product.apk:product/overlay/PixelAccessibilityMenuOverlay.apk
product/overlay/BuiltInPrintService__husky__auto_generated_rro_product.apk:product/overlay/PixelBuiltInPrintServiceOverlay.apk
product/overlay/ContactsProvider__husky__auto_generated_rro_product.apk:product/overlay/PixelContactsProviderOverlay.apk
product/overlay/Flipendo__husky__auto_generated_rro_product.apk:product/overlay/PixelFlipendoOverlay.apk
product/overlay/GoogleConfigOverlay.apk
product/overlay/GoogleWebViewOverlay.apk
product/overlay/ManagedProvisioningPixelOverlay.apk
product/overlay/PixelBatteryHealthOverlay.apk
product/overlay/PixelConfigOverlay2018.apk
product/overlay/PixelConfigOverlay2019.apk
product/overlay/PixelConfigOverlay2019Midyear.apk
product/overlay/PixelConfigOverlay2021.apk
product/overlay/PixelConfigOverlayCommon.apk
product/overlay/SettingsGoogleOverlay2021AndNewer.apk
product/overlay/SettingsGoogle__husky__auto_generated_rro_product.apk:product/overlay/PixelSettingsGoogleOverlay.apk
product/overlay/SettingsProvider__husky__auto_generated_rro_product.apk:product/overlay/PixelSettingsProviderOverlay.apk
product/overlay/StorageManagerGoogle__husky__auto_generated_rro_product.apk:product/overlay/PixelStorageManagerGoogleOverlay.apk
product/overlay/SystemUIGXOverlay.apk
product/overlay/SystemUIGoogle__husky__auto_generated_rro_product.apk:product/overlay/PixelSystemUIGoogleOverlay.apk
product/overlay/TeleService__husky__auto_generated_rro_product.apk:product/overlay/PixelTeleServiceOverlay.apk
product/overlay/Telecom__husky__auto_generated_rro_product.apk:product/overlay/PixelTelecomOverlay.apk
product/overlay/TelephonyProvider__husky__auto_generated_rro_product.apk:product/overlay/PixelTelephonyProviderOverlay.apk
product/overlay/Traceur__husky__auto_generated_rro_product.apk:product/overlay/PixelTraceurOverlay.apk
product/overlay/TrafficLightFaceOverlay.apk:product/overlay/PixelTrafficLightFaceOverlay.apk
product/overlay/UdfpsOverlay.apk:product/overlay/PixelUdfpsOverlay.apk
product/overlay/WildlifeSettingsVpnOverlay2022.apk
product/overlay/WildlifeSysuiVpnOverlay2022.apk
product/overlay/framework-res__husky__auto_generated_rro_product.apk:product/overlay/PixelFrameworkOverlay.apk

# NgaResources
product/app/NgaResources/NgaResources.apk;PRESIGNED
product/etc/sysconfig/nga.xml

# PackageInstaller
system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk;OVERRIDES=PackageInstaller;PRESIGNED

# Permissions
product/etc/default-permissions/default-permissions.xml
product/etc/default-permissions/default-permissions_diagnostictool.xml
product/etc/default-permissions/default-permissions_maestro.xml
product/etc/default-permissions/default-permissions_pixelsupport.xml
product/etc/permissions/com.android.omadm.service.xml
product/etc/permissions/com.google.SSRestartDetector.xml
product/etc/permissions/com.google.android.dialer.support.xml
product/etc/permissions/com.google.android.odad.xml
product/etc/permissions/com.google.omadm.trigger.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/permissions/split-permissions-google.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

# Phone
product/framework/com.google.android.dialer.support.jar
system_ext/priv-app/TelephonyGoogle/TelephonyGoogle.apk;PRESIGNED

# Photo
product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
product/app/Photos/Photos.apk;OVERRIDES=Gallery2;PRESIGNED

# Pixel Wireless Charger
product/etc/permissions/com.google.android.apps.dreamliner.xml
product/etc/sysconfig/dreamliner.xml
product/priv-app/DreamlinerPrebuilt/DreamlinerPrebuilt.apk;PRESIGNED
product/priv-app/DreamlinerUpdater/DreamlinerUpdater.apk;PRESIGNED

# Pixel Adaptive Charging
product/etc/sysconfig/adaptivecharging.xml

# Print
system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;OVERRIDES=PrintRecommendationService;PRESIGNED

# SafetyHub
product/priv-app/SafetyHubPrebuilt/SafetyHubPrebuilt.apk;OVERRIDES=EmergencyInfo;PRESIGNED
system_ext/app/EmergencyInfoGoogleNoUi/EmergencyInfoGoogleNoUi.apk;OVERRIDES=EmergencyInfo;PRESIGNED

# Security
product/app/DevicePolicyPrebuilt/DevicePolicyPrebuilt.apk;PRESIGNED
product/etc/security/fsverity/gms_fsverity_cert.der
product/etc/security/fsverity/play_store_fsi_cert.der
product/priv-app/WfcActivation/WfcActivation.apk;PRESIGNED

# Search Selector
product/etc/permissions/com.google.android.apps.setupwizard.searchselector.xml
product/priv-app/SearchSelectorPrebuilt/SearchSelectorPrebuilt.apk;PRESIGNED

# Setup
product/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;OVERRIDES=OneTimeInitializer;PRESIGNED
product/priv-app/GoogleRestorePrebuilt-v445524/GoogleRestorePrebuilt-v445524.apk:product/priv-app/GoogleRestorePrebuilt/GoogleRestorePrebuilt.apk;PRESIGNED
product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk;PRESIGNED
product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk;OVERRIDES=Provision;PRESIGNED
system_ext/priv-app/SetupWizardPixelPrebuilt/SetupWizardPixelPrebuilt.apk;PRESIGNED

# Sound
product/app/SoundPickerPrebuilt/SoundPickerPrebuilt.apk;PRESIGNED

# Styles & Wallpapers
product/app/PixelThemesStub/PixelThemesStub.apk;PRESIGNED
product/app/PixelThemesStub2022_and_newer/PixelThemesStub2022_and_newer.apk;PRESIGNED
product/app/PixelWallpapers2023/PixelWallpapers2023.apk;PRESIGNED
product/app/WallpaperEmojiPrebuilt-v470/WallpaperEmojiPrebuilt-v470.apk:product/app/WallpaperEmojiPrebuilt/WallpaperEmojiPrebuilt.apk;PRESIGNED
product/priv-app/AiWallpapers/AiWallpapers.apk;PRESIGNED
product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk;PRESIGNED
product/priv-app/WallpaperEffect/WallpaperEffect.apk;PRESIGNED
system_ext/priv-app/WallpaperPickerGoogleRelease/WallpaperPickerGoogleRelease.apk;OVERRIDES=ThemePicker,WallpaperPicker,WallpaperPicker2,WallpaperCropper;PRESIGNED

# Sysconfig
product/etc/sysconfig/google-install-constraints-package-allowlist.xml
product/etc/sysconfig/google-system-apps-update-ownership.xml
product/etc/sysconfig/pixel_2016_exclusive.xml|f92443f76f9af566813e08495f00c37a882af824
product/etc/sysconfig/preinstalled-packages-platform-handheld-product.xml
product/etc/sysconfig/preinstalled-packages-platform-overlays.xml
product/etc/sysconfig/preinstalled-packages-platform-telephony-product.xml
product/etc/preferred-apps/google.xml
product/etc/sysconfig/allowlist_com.android.omadm.service.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google-initial-package-stopped-states.xml
product/etc/sysconfig/google-staged-installer-whitelist.xml
product/etc/sysconfig/google.xml
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/pixel_2017-initial-package-stopped-states.xml
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2021.xml
product/etc/sysconfig/pixel_experience_2021_midyear.xml
product/etc/sysconfig/pixel_experience_2022.xml
product/etc/sysconfig/pixel_experience_2022_midyear.xml
product/etc/sysconfig/pixel_experience_2023.xml
product/etc/sysconfig/pixel_experience_2023_midyear.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2018-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2019-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2022-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2023-and-newer.xml
product/etc/sysconfig/quick_tap.xml

# Tags
system/priv-app/TagGoogle/TagGoogle.apk;OVERRIDES=Tag;PRESIGNED

# Turbo
system_ext/lib64/libpowerstatshaldataprovider.so
system_ext/priv-app/TurboAdapter/TurboAdapter.apk;PRESIGNED

# Voice
product/app/GoogleTTS/GoogleTTS.apk;OVERRIDES=PicoTts;PRESIGNED
product/app/talkback/talkback.apk;PRESIGNED
product/priv-app/RecorderPrebuilt/RecorderPrebuilt.apk;OVERRIDES=Recorder;PRESIGNED

# Wallpapers
product/wallpaper/image/default_wallpaper_BLK
product/wallpaper/image/default_wallpaper_BUE
product/wallpaper/image/default_wallpaper_GRN
product/wallpaper/image/default_wallpaper_WHT

# Weather
product/priv-app/WeatherPixelPrebuilt/WeatherPixelPrebuilt.apk;PRESIGNED

# Wellbeing
product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED

# Pixel framework
system_ext/etc/permissions/com.android.settings.xml:system_ext/etc/permissions/com.google.android.settings.xml
system_ext/etc/permissions/com.android.systemui.xml:system_ext/etc/permissions/com.google.android.systemui.xml

# Signatures (For extracting values only)
vendor/etc/selinux/vendor_mac_permissions.xml

# Pinned section

# Device Personalization Services - from raven U.0.sysimg.pixel6.557423134
product/priv-app/DevicePersonalizationPrebuiltPixel2021/DevicePersonalizationPrebuiltPixel2021.apk;PRESIGNED

# Lock screen clocks - from barbet_beta-user-14-UPB4.230623.005
system_ext/priv-app/SystemUIClocks-Handwritten/SystemUIClocks-Handwritten.apk;PRESIGNED|5285c4cb653933c886e220d11b5f8aa1517c4683

# Overlays (Copy to extraction source before extract) - from tangorpro-user-14-AP2A.240805.005-12025142-release-keys
product/overlay/SfpsOverlayTablet.apk:product/overlay/PixelSfpsOverlay.apk
