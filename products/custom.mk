# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/google/gms/products/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/google/gms/products/overlay/common

# GMS RRO overlay
PRODUCT_PACKAGES += \
    PixelConfigOverlayCustom \
    SettingsGoogleOverlayCustom \
    SystemUIGoogleOverlayCustom \
    DocumentsUIGoogleOverlayCustom \
    PixelLauncherOverlayCustom

# Launcher overlay
ifeq ($(TARGET_DEVICE_IS_TABLET),true)
PRODUCT_PACKAGES += \
    NexusLauncherTabletOverlay
endif

ifeq ($(TARGET_SUPPORTS_CLEAR_CALLING),true)
PRODUCT_PACKAGES += \
    ClearCallingOverlay
endif

ifeq ($(TARGET_SUPPORTS_DREAMLINER),true)
PRODUCT_PACKAGES += \
    DreamlinerOverlay
endif

# Custom Google apps whitelist
PRODUCT_PACKAGES += \
    custom-google-framework-sysconfig.xml \
    custom-google-hiddenapi-package-whitelist.xml \
    custom-google-platform.xml \
    custom-google-power-whitelist

# DSE search engine choice screen
ifneq ($(TARGET_EEA_V2_DEVICE),true)
ifneq ($(TARGET_EEA_V1_DEVICE),true)
PRODUCT_PACKAGES += \
    dse_choice_screen
endif
endif

# EEA
ifeq ($(TARGET_EEA_V2_DEVICE),true)
PRODUCT_PACKAGES += \
    eea_v2_search_chrome
else ifeq ($(TARGET_EEA_V1_DEVICE),true)
PRODUCT_PACKAGES += \
    eea_v1
endif

# Monet boot animation
PRODUCT_COPY_FILES += \
    vendor/google/gms/products/media/bootanimation-monet.zip:$(TARGET_COPY_OUT_PRODUCT)/media/bootanimation-monet.zip

# Themed icons for Pixel Launcher
$(call inherit-product, vendor/google/ThemeIcons/config.mk)
